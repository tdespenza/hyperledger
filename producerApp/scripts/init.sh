clear
mkdir -p ~/.hfc-key-store

cp ../crypto-config/peerOrganizations/Asus.com/users/Admin@Asus.com/msp/admincerts/Admin@Asus.com-cert.pem ./certs
cp ../crypto-config/peerOrganizations/Asus.com/users/Admin@Asus.com/msp/keystore/* ./certs
awk '{printf "%s\\n", $0}' ./certs/Admin@Asus.com-cert.pem > ./certs/AsusAdminFormatted.pem
#copy and past content of Formatted.pem file in Admin file
cp ./certs/d146a6a9e12f5601426cb3943173542383c71cc0eff2eaf2271e2247daa06204_sk ~/.hfc-key-store/d146a6a9e12f5601426cb3943173542383c71cc0eff2eaf2271e2247daa06204-priv

cp ../crypto-config/peerOrganizations/HP.com/users/Admin@HP.com/msp/admincerts/Admin@HP.com-cert.pem ./certs
cp ../crypto-config/peerOrganizations/HP.com/users/Admin@HP.com/msp/keystore/* ./certs
awk '{printf "%s\\n", $0}' ./certs/Admin@HP.com-cert.pem > ./certs/HPAdminFormatted.pem
# #copy and past content of Formatted.pem file in Admin file
cp ./certs/885a73e27f2f35c15ae131bfe515acc67a3411167bf13240fa6ff397a8b33f50_sk ~/.hfc-key-store/885a73e27f2f35c15ae131bfe515acc67a3411167bf13240fa6ff397a8b33f50-priv

cp ../crypto-config/peerOrganizations/Dell.com/users/Admin@Dell.com/msp/admincerts/Admin@Dell.com-cert.pem ./certs
cp ../crypto-config/peerOrganizations/Dell.com/users/Admin@Dell.com/msp/keystore/* ./certs
awk '{printf "%s\\n", $0}' ./certs/Admin@Dell.com-cert.pem > ./certs/DellAdminFormatted.pem
# #copy and past content of Formatted.pem file in Admin file
cp ./certs/afee6413d49fbd5d754ccc5fc98e7dae4177802236fdf633a314caee5edc57fe_sk ~/.hfc-key-store/3afee6413d49fbd5d754ccc5fc98e7dae4177802236fdf633a314caee5edc57fe-priv
