# run app node init script

clear

# join channels
echo "join channels..."
docker exec cli.Asus bash -c 'peer channel join -b asus.block'
docker exec cli.HP bash -c 'peer channel join -b hp.block'
docker exec cli.Dell bash -c 'peer channel join -b dell.block'

# update anchors
echo "update anchors..."
docker exec cli.Asus bash -c 'peer channel update -o orderer.pcxchg.com:7050 -c asus -f ./channels/asusanchor.tx'
docker exec cli.Dell bash -c 'peer channel update -o orderer.pcxchg.com:7050 -c dell -f ./channels/dellanchor.tx'
docker exec cli.HP bash -c 'peer channel update -o orderer.pcxchg.com:7050 -c hp -f ./channels/hpanchor.tx'

# install chaincodes
echo "install chaincodes..."
docker exec cli.Asus bash -c 'peer chaincode install -p producer -n producer -v 0'
docker exec cli.HP bash -c 'peer chaincode install -p producer -n producer -v 0'
docker exec cli.Dell bash -c 'peer chaincode install -p producer -n producer -v 0'

docker exec cli.Amazon bash -c 'peer chaincode install -p producer -n producer -v 0'
docker exec cli.Amazon bash -c 'peer chaincode install -p market -n market -v 0'

# instantiate chaincodes
echo "instantiate chaincodes..."
docker exec cli.Asus bash -c "peer chaincode instantiate -C asus -n producer -v 0 -c '{\"Args\":[]}'"
docker exec cli.HP bash -c "peer chaincode instantiate -C hp -n producer -v 0 -c '{\"Args\":[]}'"
docker exec cli.Dell bash -c "peer chaincode instantiate -C dell -n producer -v 0 -c '{\"Args\":[]}'"

docker exec cli.Amazon bash -c "peer chaincode instantiate -C asus -n market -v 0 -c '{\"Args\":[]}'"
docker exec cli.Amazon bash -c "peer chaincode instantiate -C hp -n market -v 0 -c '{\"Args\":[]}'"
docker exec cli.Amazon bash -c "peer chaincode instantiate -C dell -n market -v 0 -c '{\"Args\":[]}'"

# create PCs
# echo "create PCs..."
docker exec cli.Asus bash -c "peer chaincode invoke -C asus -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Asus001\", \"foo\", \"bar\"]}'"
docker exec cli.Asus bash -c "peer chaincode invoke -C asus -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Asus002\", \"foo\", \"bar\"]}'"
docker exec cli.Asus bash -c "peer chaincode invoke -C asus -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Asus003\", \"foo\", \"bar\"]}'"

docker exec cli.HP bash -c "peer chaincode invoke -C hp -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"HP001\", \"foo\", \"bar\"]}'"
docker exec cli.HP bash -c "peer chaincode invoke -C hp -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"HP002\", \"foo\", \"bar\"]}'"

docker exec cli.Dell bash -c "peer chaincode invoke -C dell -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Dell001\", \"foo\", \"bar\"]}'"
docker exec cli.Dell bash -c "peer chaincode invoke -C dell -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Dell002\", \"foo\", \"bar\"]}'"
docker exec cli.Dell bash -c "peer chaincode invoke -C dell -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Dell003\", \"foo\", \"bar\"]}'"
docker exec cli.Dell bash -c "peer chaincode invoke -C dell -n pcxchg -v 0 -c '{\"Args\":[\"createPC\", \"Dell004\", \"foo\", \"bar\"]}'"

yarn install
node producerApp.js

# curl -s -X POST  http://localhost:4000/invoke -H "content-type: application/json" -d '{"args":["Asus","Asus001","foo","bar"]}'
#
# docker exec cli.Amazon bash -c "peer chaincode query -C asus -n pcxchg -v 0 -c '{\"Args\":[\"queryStock\"]}'"
# docker exec cli.Amazon bash -c "peer chaincode query -C hp -n pcxchg -v 0 -c '{\"Args\":[\"queryStock\"]}'"
# docker exec cli.Amazon bash -c "peer chaincode query -C dell -n pcxchg -v 0 -c '{\"Args\":[\"queryStock\"]}'"
#
# node ./producerApp/producerApp.js
